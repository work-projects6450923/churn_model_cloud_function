from oauth2client.client import GoogleCredentials
from googleapiclient import discovery
from googleapiclient import errors
from google.cloud import pubsub_v1
import uuid
def hello_cd(event, context):
    import base64
    import json
    inputs = {
        "scaleTier": "BASIC",
        "region": "us-central1",
        "masterConfig": {
            "imageUri": "gcr.io/path/customer_defection:v0.0.1"
        }
    }
    job_spec = {
        'jobId':'prop_subro_run_'+str(uuid.uuid1()).replace('-',''),
        'trainingInput':inputs
    }
    project_id = 'projects/gcp-cah-datascience-dev'
    ml = discovery.build('ml','v1', cache_discovery=False) 
    # https://github.com/googleapis/google-api-python-client/issues/299#issuecomment-268915510
    
    ml.projects().jobs().create(
        body=job_spec,parent=project_id
    ).execute()
