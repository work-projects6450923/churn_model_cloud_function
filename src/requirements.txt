cachetools==4.2.1
    # via google-auth
certifi==2020.12.5
    # via requests
chardet==4.0.0
    # via requests
google-api-core[grpc]==1.25.1
    # via
    #   google-api-python-client
    #   google-cloud-pubsub
google-api-python-client==1.12.8
    # via -r requirements.in
google-auth-httplib2==0.0.4
    # via google-api-python-client
google-auth==1.24.0
    # via
    #   google-api-core
    #   google-api-python-client
    #   google-auth-httplib2
google-cloud-pubsub==2.2.0
    # via -r requirements.in
googleapis-common-protos[grpc]==1.52.0
    # via
    #   google-api-core
    #   grpc-google-iam-v1
grpc-google-iam-v1==0.12.3
    # via google-cloud-pubsub
grpcio==1.35.0
    # via
    #   google-api-core
    #   googleapis-common-protos
    #   grpc-google-iam-v1
httplib2==0.18.1
    # via
    #   google-api-python-client
    #   google-auth-httplib2
    #   oauth2client
idna==2.10
    # via requests
libcst==0.3.16
    # via google-cloud-pubsub
mypy-extensions==0.4.3
    # via typing-inspect
oauth2client==4.1.3
    # via -r requirements.in
proto-plus==1.13.0
    # via google-cloud-pubsub
protobuf==3.14.0
    # via
    #   google-api-core
    #   googleapis-common-protos
    #   proto-plus
pyasn1-modules==0.2.8
    # via
    #   google-auth
    #   oauth2client
pyasn1==0.4.8
    # via
    #   oauth2client
    #   pyasn1-modules
    #   rsa
pytz==2020.5
    # via google-api-core
pyyaml==5.4.1
    # via libcst
requests==2.25.1
    # via google-api-core
rsa==4.7
    # via
    #   google-auth
    #   oauth2client
six==1.15.0
    # via
    #   google-api-core
    #   google-api-python-client
    #   google-auth
    #   google-auth-httplib2
    #   grpcio
    #   oauth2client
    #   protobuf
typing-extensions==3.7.4.3
    # via
    #   libcst
    #   typing-inspect
typing-inspect==0.6.0
    # via libcst
uritemplate==3.0.1
    # via google-api-python-client
urllib3==1.26.3
    # via requests